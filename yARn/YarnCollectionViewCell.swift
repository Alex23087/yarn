//
//  YarnCollectionViewCell.swift
//  yARn
//
//  Created by Alessandro Scala on 10/12/2018.
//  Copyright © 2018 TheLeftSide. All rights reserved.
//

import UIKit

class YarnCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var yarnName: UILabel!
    @IBOutlet weak var yarnView: UIImageView!
    
}
