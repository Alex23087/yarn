//
//  YarnData.swift
//  yARn
//
//  Created by Alessandro Scala on 10/12/2018.
//  Copyright © 2018 TheLeftSide. All rights reserved.
//

import Foundation

class YarnUserData: Codable{
    var userName: String!
    var hunger: Float!
    var fun: Float!
    var cookieThrows: Int!
    var timesPetted: Int!
    
    static func ==(rhs: YarnUserData, lhs: YarnUserData) -> Bool{
        if rhs.userName != lhs.userName{return false}
        if rhs.hunger != lhs.hunger{return false}
        if rhs.fun != lhs.fun{return false}
        return true
    }
    
    init(name: String){
        userName = name
        hunger = 0.0
        fun = 0.0
        cookieThrows = 0
        timesPetted = 0
    }
}

public class YarnData: Codable{
    var name: String!
    var users: [YarnUserData]!
    var timestamp: String!
    
    var userNames: [String]{
        return users.map({$0.userName})
    }
    
    init(name: String, users: [YarnUserData]){
        self.name = name
        self.users = users
        timestamp = Date(timeIntervalSinceNow: 0).description
    }
    
    func touch(){
        timestamp = Date(timeIntervalSinceNow: 0).description
    }
    
    func timestampAsDate() -> Date{
        var dateFormatter = DateFormatter()
        dateFormatter.dateFormat =  "yyyy-MM-dd HH:mm:ss +zzzz"
        return dateFormatter.date(from: timestamp)!
    }
    
    static func equals(rhs: YarnData, lhs: YarnData, permissive: Bool = true) -> Bool{
        if rhs.name != lhs.name{return false}
        if rhs.users.count != lhs.users.count{return false}
        if !rhs.users.elementsEqual(lhs.users, by: { (right, left) -> Bool in
            right.userName == left.userName
        }){
            return false
        }
        if !permissive{
            if !rhs.users.elementsEqual(lhs.users, by: { (right, left) -> Bool in
                right.cookieThrows == left.cookieThrows && right.fun == left.fun && right.hunger == left.hunger && right.timesPetted == left.timesPetted
            }){
                return false
            }
        }
        return true
    }
    
    static func ==(rhs: YarnData, lhs: YarnData) -> Bool{
        return equals(rhs: rhs, lhs: lhs, permissive: false)
    }
}
