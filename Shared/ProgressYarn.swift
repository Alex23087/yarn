//
//  Progress.swift
//  yARn
//
//  Created by Ole Werger on 13.12.18.
//  Copyright © 2018 TheLeftSide. All rights reserved.
//

import Foundation

class ProgressYarn: Codable {
    
    var fun: Float = 0.0
    var food: Float = 0.0
    
    init(fun: Float, food: Float){
        self.fun = fun
        self.food = food
    }
    
    convenience init(){
        self.init(fun: 0.0, food: 0.0)
    }
    
    func setProgress(fun: Float, food: Float){
        guard let path = Bundle.main.path(forResource: "progress", ofType: "json") else { return }
        
        let data = try? JSONEncoder().encode(ProgressYarn(fun: fun, food: food))
        
        if let file = FileHandle(forWritingAtPath: path) {
            file.write(data!)
        }
        
    }
    
    
    func getProgress() -> ProgressYarn{
    
        guard let path = Bundle.main.path(forResource: "progress", ofType: "json"),
        let data = try? Data(contentsOf: URL(fileURLWithPath: path)) else {
        return ProgressYarn(fun: 0.0, food: 0.0)
        }
        let object = try? JSONDecoder().decode(ProgressYarn.self, from: data)
        
        return object!
    }
    
}
