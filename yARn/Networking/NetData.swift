//
//  NetData.swift
//  SessionShare Test
//
//  Created by Alessandro Scala on 06/12/2018.
//  Copyright © 2018 Alessandro Scala. All rights reserved.
//

import SceneKit

public class CookieThrow: Codable{
    private var _position: CodableFloat4x4
    private var _direction: CodableFloat4x4
    var velocity: Float
    var position: float4x4 {return _position.toTransform()}
    var direction: float4x4 {return _direction.toTransform()}
    
    init(position: float4x4, direction: float4x4, velocity: Float = 0.3){
        self._position = CodableFloat4x4(from: position)
        self._direction = CodableFloat4x4(from: direction)
        self.velocity = velocity
    }
    
    //    var directionVector: SCNVector3{
    //        return SCNVector3(x: direction.x3, y: direction.y3, z: direction.z3)
    //    }
}

class CodableFloat4x4: Codable{
    var w0: Float
    var x0: Float
    var y0: Float
    var z0: Float
    
    var w1: Float
    var x1: Float
    var y1: Float
    var z1: Float
    
    var w2: Float
    var x2: Float
    var y2: Float
    var z2: Float
    
    var w3: Float
    var x3: Float
    var y3: Float
    var z3: Float
    
    init(from: float4x4){
        w0 = from.columns.0.w
        x0 = from.columns.0.x
        y0 = from.columns.0.y
        z0 = from.columns.0.z
        
        w1 = from.columns.1.w
        x1 = from.columns.1.x
        y1 = from.columns.1.y
        z1 = from.columns.1.z
        
        w2 = from.columns.2.w
        x2 = from.columns.2.x
        y2 = from.columns.2.y
        z2 = from.columns.2.z
        
        w3 = from.columns.3.w
        x3 = from.columns.3.x
        y3 = from.columns.3.y
        z3 = from.columns.3.z
    }
    
    func toTransform() -> float4x4{
        var out = float4x4()
        
        out.columns.0.w = w0
        out.columns.0.x = x0
        out.columns.0.y = y0
        out.columns.0.z = z0
        
        out.columns.1.w = w1
        out.columns.1.x = x1
        out.columns.1.y = y1
        out.columns.1.z = z1
        
        out.columns.2.w = w2
        out.columns.2.x = x2
        out.columns.2.y = y2
        out.columns.2.z = z2
        
        out.columns.3.w = w3
        out.columns.3.x = x3
        out.columns.3.y = y3
        out.columns.3.z = z3
        
        return out
    }
}

class CodableFloat3: Codable{
    var x: Float
    var y: Float
    var z: Float
    
    init(from: float3){
        x = from.x
        y = from.y
        z = from.z
    }
    
    init(from: SCNVector3){
        x = from.x
        y = from.y
        z = from.z
    }
    
    func toFloat3() -> float3{
        var out = float3()
        out.x = x
        out.y = y
        out.z = z
        return out
    }
    
    func toSCNVector3() -> SCNVector3{
        return SCNVector3(x, y, z)
    }
}
