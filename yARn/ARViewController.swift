//
//  ARViewController.swift
//  yARn
//
//  Created by Alessandro Scala on 10/12/2018.
//  Copyright © 2018 TheLeftSide. All rights reserved.
//

import UIKit
import ARKit
import MultipeerConnectivity
import AudioToolbox
import AVFoundation

class ARViewController: UIViewController, ARSCNViewDelegate, GameNetworkDelegate, YarnDelegate {
    func getPosition() -> ARAnchor {
        return yarnPosition
    }
    
    var world: ARWorldMap?
    var __yarnPosition: ARAnchor?
    @IBOutlet weak var funIcon: UIImageView!
    let SCALING = SCNVector3(x: 0.01, y: 0.01, z: 0.01)
    let COOKIE_ANIMATION_DURATION = 2.0
    @IBOutlet weak var foodIcon: UIImageView!
    @IBOutlet weak var cookieBtn: UIButton!
    var yarnNode: SCNNode!
    var yarnSkeleton: SCNNode!
    var planeNode: SCNNode!
    var yarnPosition: ARAnchor!{
        get{
            return _yarnPosition
        }
        set(value){
            if value == nil{
                self.netSession.stopAdvertising()
            }else{
                if case ARCamera.TrackingState.normal = self.sceneView.session.currentFrame!.camera.trackingState{  //Oh god, I thought swift syntax was good, but using one equals to check for equality is a no-no
                    self.netSession.startAdvertising()
                }
            }
            if _yarnPosition != nil{
                self.sceneView.session.remove(anchor: _yarnPosition)
            }
            _yarnPosition = value
            self.sceneView.session.add(anchor: value)
        }
    }
    private var _yarnPosition: ARAnchor!
    var isJoining = false
    let maxCookiesSingle = 4
    var maxCookiesTotal: Int{
        return maxCookiesSingle * yarn.users.count
    }
    private var hungerTotal: Float{
        return yarn.users.reduce(into: 0.0, { (sum, user) in sum += user.hunger}) / Float(yarn.users.count)
    }
    private var funTotal: Float{
        return yarn.users.reduce(into: 0.0, { (sum, user) in sum += user.fun}) / Float(yarn.users.count)
    }
    var player: AVAudioPlayer!
    
    var pY = ProgressYarn()
    
    func set(hunger: Float, player: MCPeerID) {
        //        print("\(hunger) \(player)")
        //        let dialog = UIAlertController(title: "Test", message: "\(hunger) \(player)", preferredStyle: .alert)
        //        dialog.addAction(UIAlertAction(title: "Ok", style: .default, handler: nil))
        //        self.present(dialog, animated: true, completion: nil)
        
        if feedList[player.displayName] == nil{
            feedList[player.displayName] = 0
        }
        
        if feedList.count>0 {
//            print("[LOG] FeedList: \(feedList)")
//            if feedList[player.displayName]!<maxCookiesSingle {
            let user = yarn.users.first(where: {$0.userName == player.displayName})!
            user.cookieThrows += 1
            user.hunger += hunger
            
            DispatchQueue.main.async {
                self.pY.setProgress(fun: self.funTotal, food: self.hungerTotal)
                self.progressBarFood.setProgress(self.hungerTotal, animated: true)
                if(self.progressBarFood.progress <= 0.5){
                    self.progressBarFood.tintColor = UIColor.red
                }
                else {
                    self.progressBarFood.tintColor = UIColor.green
                }
            }
            if player.displayName != self.netSession.myPeerID.displayName{
                self.feedList[player.displayName] = self.feedList[player.displayName]! + 1
            }
//            }
            self.playEatingAnimation()
        }
    }
    
    let maxFunSingle = 4
    var maxFunTotal: Int{
        return maxFunSingle * yarn.users.count
    }
    func set(fun: Float, player: MCPeerID) {
        //        let dialog = UIAlertController(title: "Test", message: "\(fun) \(player) \(funList[player.displayName])", preferredStyle: .alert)
        //        dialog.addAction(UIAlertAction(title: "Ok", style: .default, handler: nil))
        //        self.present(dialog, animated: true, completion: nil)
        if funList[player.displayName] == nil{
            funList[player.displayName] = 0
        }
        
        let user = self.yarn.users.first(where: {$0.userName == player.displayName})!
        user.timesPetted += 1
        user.fun += fun
        DispatchQueue.main.async {
            
//        if self.funList.count>0 && self.funList[player.displayName]!<self.maxFunSingle {
            self.pY.setProgress(fun: self.funTotal, food: self.hungerTotal)
            self.progressBarFun.setProgress(self.funTotal, animated: true)
            if(self.progressBarFun.progress <= 0.5){
                self.progressBarFun.tintColor = UIColor.red
            }
            else {
                self.progressBarFun.tintColor = UIColor.green
            }
            if player.displayName != self.netSession.myPeerID.displayName{
                self.funList[player.displayName] = self.funList[player.displayName]! + 1
            }
            self.playScratchingAnimation()
        }
//        }
    }
    
    func setYarn(data: YarnData) {
        print("[LOG] SetYarn called")
        self.yarn = data
        print("[LOG] yarn.usernames: \(yarn.userNames)")
        var newFeed = [String: Int]()
        var newPet = [String: Int]()
        for user in yarn.users{
            newFeed[user.userName] = user.cookieThrows
            newPet[user.userName] = user.timesPetted
        }
        feedList = newFeed
        funList = newPet
        //Resetting progress bars with the sum of the hunger and fun values divided by the number of users
        DispatchQueue.main.async {
            self.progressBarFood.setProgress(self.yarn.users.reduce(into: 0.0, { (res, user) in res += user.hunger})/Float(self.yarn.users.count), animated: false)
            self.progressBarFun.setProgress(self.yarn.users.reduce(into: 0.0, { (res, user) in res += user.fun})/Float(self.yarn.users.count), animated: false)
        }
        isJoining = false
    }
    
    func getYarn() -> YarnData? {
        return yarn
    }
    
    
    
    
    func setWorld(map: ARWorldMap) {
        let configuration = ARWorldTrackingConfiguration()
        configuration.initialWorldMap = map
        configuration.planeDetection = .horizontal
        sceneView.session.run(configuration)
    }
    
    func getWorld(callback: @escaping(ARWorldMap) -> Void){
        sceneView.session.getCurrentWorldMap { (map, error) in
            guard let worldMap = map else {fatalError((error?.localizedDescription)!)}
            print("[LOG] getWorld() is returning")
            callback(worldMap)
        }
    }
    
    //var donuts: [SCNNode] = []
    func throwCookie(action: CookieThrow) {
        let donut = SCNScene(named: "art.scnassets/Cookie Model (Needs to add texture from scene)/Cookie.scn")!
        
        weak var node = donut.rootNode.childNode(withName: "Cookie", recursively: true)!
        node?.scale = SCALING
        sceneView.scene.rootNode.addChildNode(node!)
        //donuts.append(node)
        
        let positionShip = action.direction.translation
        let positionDonut = action.position.translation
        
        //var index = donuts.count - 1
        node?.position = positionDonut
        
        let move = SCNAction.move(to: positionShip, duration: COOKIE_ANIMATION_DURATION)
        node?.runAction(move)
        
        DispatchQueue.main.async {
            Timer.scheduledTimer(withTimeInterval: self.COOKIE_ANIMATION_DURATION, repeats: false) { (_) in
                //if index >= self.donuts.count {index = self.donuts.count - 1}
                node?.removeFromParentNode()
                node?.isHidden = true
                node = nil
            }
        }
    }
    
    func setYarnPosition(position: ARAnchor) {
        yarnPosition = position     //Doing this removes the previous anchor sets the new one, and moves the yarn object. Oh, computed properties are a great thing!
    }
    
    var timerAttempts = 0
    
    func connectionRequest(from peer: MCPeerID, to session: MCSession, handler: @escaping (Bool, MCSession) -> Void) {
        if yarn.userNames.contains(peer.displayName){
            handler(true, session)
        }else{
            let dialog = UIAlertController(title: "\(peer.displayName) is trying to connect!", message: "Do you want to add him/her to your yarn?", preferredStyle: .alert)
            dialog.addAction(UIAlertAction(title: "No", style: .cancel, handler: {_ in
                handler(false, session)
            }))
            dialog.addAction(UIAlertAction(title: "Yes", style: .default, handler: {_ in
                handler(true, session)
                self.netSession.invite(peer: peer)
                self.yarn.users.append(YarnUserData(name: peer.displayName))
                self.timerAttempts = 10
                Timer.scheduledTimer(withTimeInterval: 0.5, repeats: true, block: { (timer) in
                    self.netSession.sendConnectionEstablished()
                    self.timerAttempts -= 1
                    if self.timerAttempts < 1 {
                        timer.invalidate()
                    }
                    /*self.sceneView.session.getCurrentWorldMap(completionHandler: { (map, error) in
                        guard map != nil else{print(error!.localizedDescription); return}
                        self.netSession.send(worldMap: map!)
                    })
                    self.netSession.send(yarn: self.yarn)
                    self.netSession.send(yarnPosition: self.yarnPosition)*/
                })
                DispatchQueue.main.async {
                    self.progressBarFun.setProgress((self.progressBarFun.progress / Float(self.yarn.users.count) * Float(self.yarn.users.count-1)), animated: true)
                    self.progressBarFood.setProgress((self.progressBarFood.progress / Float(self.yarn.users.count) * Float(self.yarn.users.count-1)), animated: true)
                }
            }))
            self.present(dialog, animated: true, completion: nil)
        }
        /*for player in netSession.connectedPeers {
            feedList[player.displayName] = 0
            funList[player.displayName] = 0
            
        }
        
        progressBarFun.progress = 0.0
        progressBarFood.progress = 0.0
        
        print("\(feedList)")
        print("\(funList)")*/
    }
    
    
    var netSession: YarnMultipeerSession!
    var yarn: YarnData!
    
    @IBOutlet weak var sceneView: ARSCNView!
    @IBOutlet weak var progressBarFun: UIProgressView!
    @IBOutlet weak var progressBarFood: UIProgressView!
    
    var feedList: [String:Int] = [:]
    var funList: [String:Int] = [:]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Set the view's delegate
        sceneView.delegate = self
        
        // Show statistics such as fps and timing information
        //sceneView.showsStatistics = true
        
        // Create a new scene
        let scene = SCNScene(named: "art.scnassets/Scene2.scn")!
        
        //sceneView.debugOptions = [ARSCNDebugOptions.showFeaturePoints, .showBoundingBoxes]
        // Set the scene to the view
        sceneView.scene = scene
        
        //Prevent the phone from sleeping
        UIApplication.shared.isIdleTimerDisabled = true
        
        yarnNode = SCNScene(named: "art.scnassets/yarn.scn")!.rootNode.childNode(withName: "Yarn", recursively: true)!
        yarnSkeleton = yarnNode.childNode(withName: "Corpo", recursively: true)
        //planeNode = scene.rootNode.childNode(withName: "Plane", recursively: true)!
        scene.rootNode.addChildNode(yarnNode)
        yarnNode.scale = SCALING
        yarnNode.isHidden = true
        //planeNode.isHidden = true
        
        progressBarFun.progress = 0.0
        progressBarFood.progress = 0.0
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        if world == nil{
            let configuration = ARWorldTrackingConfiguration()
            configuration.planeDetection = .horizontal
            sceneView.session.run(configuration)
        }else{
            setWorld(map: world!)
        }
        
        netSession.delegate = self
        netSession.yarnDelegate = self
        netSession.synchronizeYarnData()
        print("[LOG] connected peers: \(netSession.connectedPeers.map({$0.displayName}))")
        if isJoining{
            netSession.requestWorld()
        }
        feedList[netSession.myPeerID.displayName] = 0
        funList[netSession.myPeerID.displayName] = 0
        print("[LOG] yarn.usernames: \(yarn.userNames)")
        do{
            player = try AVAudioPlayer(contentsOf: URL(fileURLWithPath: Bundle.main.path(forResource: "yarnmusic", ofType: "mp3")!))
        }catch{
            print("[LOG] Player didn't initialize")
        }
        player.numberOfLoops = Int.max
        player.volume = 0.4
        player.play()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        if __yarnPosition != nil{
            yarnPosition = __yarnPosition
        }
        setYarn(data: yarn)     //This is so that if you're loading the yarn the progress bars are updated
        yarnSleeping = netSession.connectedPeers.count == 0
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        // Pause the view's session
        sceneView.session.pause()
    }
    
    func session(_ session: ARSession, didFailWithError error: Error) {
        // Present an error message to the user
        
    }
    
    func sessionWasInterrupted(_ session: ARSession) {
        // Inform the user that the session has been interrupted, for example, by presenting an overlay
        
    }
    
    func sessionInterruptionEnded(_ session: ARSession) {
        // Reset tracking and/or remove existing anchors if consistent tracking is required
        
    }
    
    
    var initialCenter = CGPoint()
    @IBAction func panDetected(_ sender: UIPanGestureRecognizer) {
        if !yarnSleeping{
            let translation = sender.translation(in: self.sceneView)
            if sender.state == .began {
                self.initialCenter = translation
            }
            if sender.state == .ended {
                let tapLocation = sender.location(in: sceneView)
                let hitTest = sceneView.hitTest(tapLocation, options: [.backFaceCulling: (Any).self])
                if !hitTest.isEmpty{
                    if(initialCenter.y+translation.y>140){
                        if funList[netSession.myPeerID.displayName] == nil{
                            funList[netSession.myPeerID.displayName] = 0
                        }
                        if funList[netSession.myPeerID.displayName]! < maxFunSingle{
                            AudioServicesPlayAlertSound(SystemSoundID(kSystemSoundID_Vibrate))
                            set(fun: 1.0/Float(maxFunSingle), player: netSession.myPeerID)
                            netSession.send(fun: 1.0/Float(maxFunSingle))
                            funList[netSession.myPeerID.displayName] = funList[netSession.myPeerID.displayName]! + 1
                        }
                    }
                }
            }
        }
    }
    
//    @IBAction func resetYarn(_ sender: Any) {
//        guard let hit = sceneView.hitTest(CGPoint(x: view.frame.midX, y: view.frame.midY), types: [.existingPlaneUsingGeometry, .estimatedHorizontalPlane]).first else { return }
//        let anchor = ARAnchor(transform: hit.worldTransform)
//        if anchor is ARPlaneAnchor{
//            yarnPosition = anchor
//            netSession.send(yarnPosition: anchor)
//        }
//    }
    
    func renderer(_ renderer: SCNSceneRenderer, didAdd node: SCNNode, for anchor: ARAnchor) {
        if yarnPosition == nil{
            yarnPosition = anchor
        }
        if anchor == yarnPosition{
            yarnNode.position = anchor.transform.translation
            yarnNode.simdEulerAngles.y = sceneView.session.currentFrame!.camera.eulerAngles.y - .pi/2
            yarnNode.isHidden = false
//            planeNode.position = anchor.transform.translation
//            planeNode.scale = SCNVector3(x: 0.5, y: 0.5, z: 0.5)
//            planeNode.position.x -= 1
//            planeNode.position.z -= 1
//            planeNode.isHidden = false
            guard let planeAnchor = anchor as? ARPlaneAnchor else {return}
            let width = CGFloat(planeAnchor.extent.x)
            let height = CGFloat(planeAnchor.extent.z)
            let plane = SCNPlane(width: width, height: height)

            plane.materials.first?.diffuse.contents = UIColor.blue.withAlphaComponent(0.00000000000001)

            let planeNode = SCNNode(geometry: plane)

            let x = CGFloat(planeAnchor.center.x)
            let y = CGFloat(planeAnchor.center.y)
            let z = CGFloat(planeAnchor.center.z)
            planeNode.position = SCNVector3(x,y,z)
            planeNode.eulerAngles.x = -.pi/2

            node.addChildNode(planeNode)
        }
    }
    
    @IBAction func throwFood(_ sender: UIButton) {
        if !yarnSleeping{
            let position = sceneView.pointOfView?.simdWorldTransform
            let direction = yarnNode.simdWorldTransform
            
            
            let cookie = CookieThrow(position: position!, direction: direction)
            
            if feedList[netSession.myPeerID.displayName] == nil{
                feedList[netSession.myPeerID.displayName] = 0
            }
            if feedList[netSession.myPeerID.displayName]! < maxCookiesSingle{
                netSession.send(cookieThrow: cookie)
                throwCookie(action: cookie)
                Timer.scheduledTimer(withTimeInterval: COOKIE_ANIMATION_DURATION, repeats: false) { (_) in
                    self.set(hunger: 1.0/Float(self.maxCookiesSingle), player: self.netSession.myPeerID)
                    self.netSession.send(hunger: 1.0/Float(self.maxCookiesSingle))
                }
                feedList[netSession.myPeerID.displayName] = feedList[netSession.myPeerID.displayName]! + 1
            }
        }
    }
    
    @IBAction func questionMarkPressed(_ sender: UIButton) {
        
        let dialog = UIAlertController(title: "How do I increase the fun bar?", message: "Scratch the yARn", preferredStyle: .alert)
        dialog.addAction(UIAlertAction(title: "Ok", style: .default, handler: nil))
        self.present(dialog, animated: true, completion: nil)
        
    }
    
    func shouldInvite(peer: MCPeerID) -> Bool {
        return true //yarn.userNames.contains(peer.displayName)
    }
    
    func session(_ session: ARSession, cameraDidChangeTrackingState camera: ARCamera) {
        switch camera.trackingState {
        case .normal:
            if yarnPosition != nil{
                netSession.startAdvertising()
            }
        default:
            netSession.stopAdvertising()
        }
    }
    
    func peerCountChanged(count: Int) {
        if count == 0{
            yarnSleeping = true
        }else{
            yarnSleeping = false
        }
    }
    
    var yarnSleeping: Bool{
        get{
            return _yarnSleeping ?? true
        }
        set(value){
            if value != _yarnSleeping{
                if value == true{
                    playSleepAnimation()
                    DispatchQueue.main.async {
                        self.cookieBtn.isHidden = true
                    }
                }else {
                    playAwakingAnimation()
                    DispatchQueue.main.async {
                        self.cookieBtn.isHidden = false
                    }
                }
                _yarnSleeping = value
            }
        }
    }
    var _yarnSleeping: Bool?
 
    let ANIMATION_BLENDING = CGFloat(0.1) 
    //Animations
    
    func playEatingAnimation(){
        yarnSkeleton.removeAnimation(forKey: "Idle", blendOutDuration: ANIMATION_BLENDING)
        let animation = animationFromScene(named: "art.scnassets/Animations/Eating.dae")
        yarnSkeleton.addAnimation(animation!, forKey: "Eating")
        DispatchQueue.main.async {
            Timer.scheduledTimer(withTimeInterval: (animation?.duration)!, repeats: false) { (_) in
                self.playIdleAnimation()
            }
        }
    }
    
    var purrPlayer: AVAudioPlayer!
    func playScratchingAnimation(){
        yarnSkeleton.removeAnimation(forKey: "Idle", blendOutDuration: ANIMATION_BLENDING)
        let animation = animationFromScene(named: "art.scnassets/Animations/Scratching.dae")
        yarnSkeleton.addAnimation(animation!, forKey: "Scratching")
        purrPlayer = try! AVAudioPlayer(contentsOf: URL(fileURLWithPath: Bundle.main.path(forResource: "art.scnassets/purr", ofType: "mp3")!))
        purrPlayer.numberOfLoops = 1
        purrPlayer.play()
        purrPlayer.volume = 1
        DispatchQueue.main.async {
            Timer.scheduledTimer(withTimeInterval: (animation?.duration)!, repeats: false) { (_) in
                self.playIdleAnimation()
            }
        }
    }
    
    func playAwakingAnimation(){
        yarnSkeleton.removeAnimation(forKey: "Sleeping", blendOutDuration: ANIMATION_BLENDING)
        let animation = animationFromScene(named: "art.scnassets/Animations/Connected.dae")
        yarnSkeleton.addAnimation(animation!, forKey: "Awaking")
        yarnSkeleton.removeAnimation(forKey: "Sleeping", blendOutDuration: ANIMATION_BLENDING)
        DispatchQueue.main.async {
            Timer.scheduledTimer(withTimeInterval: (animation?.duration)!, repeats: false) { (_) in
                self.playIdleAnimation()
            }
        }
    }
    
    func playIdleAnimation(){
        let animation = animationFromScene(named: "art.scnassets/Animations/Idle.dae")
        yarnSkeleton.addAnimation(animation!, forKey: "Idle")
        yarnSkeleton.animationKeys.forEach { (name) in
            if name != "Idle" {
                self.yarnNode.removeAnimation(forKey: name, blendOutDuration: ANIMATION_BLENDING)
            }
        }
    }
    
    func playSleepAnimation(){
        yarnSkeleton.animationKeys.forEach { (name) in
            self.yarnSkeleton.removeAnimation(forKey: name, blendOutDuration: ANIMATION_BLENDING)
        }
        let animation = animationFromScene(named: "art.scnassets/Animations/Waiting.dae")
        yarnSkeleton.addAnimation(animation!, forKey: "Sleeping")
    }
    
    private func animationFromScene(named name: String) -> CAAnimation? {
        let scene = SCNScene(named: name)
        var animation: CAAnimation?
            scene?.rootNode.enumerateChildNodes({ (child, stop) in
                if let key = child.animationKeys.first {
                    print(key)
                    animation = child.animation(forKey: key)
                    stop.pointee = true
                }
            })
//        animation?.isRemovedOnCompletion = true
//        animation?.fadeInDuration = ANIMATION_BLENDING
//        animation?.fadeOutDuration = ANIMATION_BLENDING
        return animation
    }
}
