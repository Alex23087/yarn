//
//  WatchInterfaceController.swift
//  yARn
//
//  Created by Ole Werger on 13.12.18.
//  Copyright © 2018 TheLeftSide. All rights reserved.
//

import WatchKit
import Foundation


class WatchInterfaceController: WKInterfaceController {

    @IBOutlet weak var yarnImage: WKInterfaceImage!
    @IBOutlet weak var foodImage: WKInterfaceImage!
    @IBOutlet weak var funImage: WKInterfaceImage!
    @IBOutlet weak var funLabel: WKInterfaceLabel!
    @IBOutlet weak var foodLabel: WKInterfaceLabel!
    
    
    var progress: ProgressYarn? {
        didSet {
            guard let progress = progress else { return }
            
            funLabel.setText("\(progress.fun)%")
            foodLabel.setText("\(progress.food)%")
        }
    }
    
    override func awake(withContext context: Any?) {
        super.awake(withContext: context)
        
        // Configure interface objects here.
        progress = ProgressYarn(fun: 0.0, food: 0.0)
    }

    override func willActivate() {
        // This method is called when watch view controller is about to be visible to user
        super.willActivate()

    }

    override func didDeactivate() {
        // This method is called when watch view controller is no longer visible
        super.didDeactivate()
    }

}
