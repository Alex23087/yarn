//
//  JoinViewController.swift
//  yARn
//
//  Created by Alessandro Scala on 10/12/2018.
//  Copyright © 2018 TheLeftSide. All rights reserved.
//

import UIKit
import MultipeerConnectivity
import ARKit

class JoinViewController: UIViewController {

    @IBOutlet weak var collectionView: UICollectionView!
    var netSession: YarnMultipeerSession!
    var yarn: YarnData?
    var world: ARWorldMap?
    var yarnPosition: ARAnchor?
    var connection = false
    var connectingAlert: UIAlertController?
    var connectedYarnName: String?
    //var joined = false
    
    var peerList = [MCPeerID: String]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        collectionView.dataSource = self
        collectionView.delegate = self
        netSession = YarnMultipeerSession(with: .firstJoin, yarnName: "", hosting: false)
        netSession.delegate = self
        netSession.yarnDelegate = self
        //yarn = YarnData(name: "", users: [])
    }
    
    /*@IBAction func join(_ sender: Any) {
        if joined{
            performSegue(withIdentifier: "ToAR", sender: self)
        }
    }*/
    
    @IBAction func backBtnPressed(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
        netSession.stopAdvertising()
        netSession = nil
    }
}

extension JoinViewController: PeerBrowserDelegate{
    func peerCountChanged(count: Int) {
        
    }
    
    func foundPeer(peerID: MCPeerID, hosting: Bool, yarnName: String) {
        if hosting{
            peerList[peerID] = yarnName
            //Timer.scheduledTimer(withTimeInterval: 1, repeats: false) { (_) in
                DispatchQueue.main.async {
                    self.collectionView.reloadData()
                }
            //}
        }
    }
    
    func lostPeer(peerID: MCPeerID) {
        peerList[peerID] = nil
        collectionView.reloadData()
    }
    
    
    func connectionEstablished(){
        /*let dialog = UIAlertController(title: "Connected", message: nil, preferredStyle: .alert)
        dialog.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
        self.present(dialog, animated: true, completion: nil)
        joined = true*/
        /*DispatchQueue.main.async {
            self.performSegue(withIdentifier: "ToAR", sender: self)
        }*/
        connectingAlert = UIAlertController(title: "Connecting", message: "Wait just one second, we're telling \(connectedYarnName ?? "ERROR") he has a new friend!", preferredStyle: .alert)
        self.present(connectingAlert!, animated: true, completion: nil)
        
        if !connection{
            print("[LOG] connectionEstablished() inside JoinViewController")
            DispatchQueue.main.async {
                Timer.scheduledTimer(withTimeInterval: 1, repeats: true) { (timer) in
                    print("[LOG] connectionEstablished timer fired")
                    print("[LOG] yarn: \(String(describing: self.yarn))")
                    print("[LOG] world: \(String(describing: self.world))")
                    if self.yarn == nil{
                        self.netSession.synchronizeYarnData()
                        print("[LOG] JoinViewController requesting yarn")
                    }
                    if self.world == nil{
                        self.netSession.requestWorld()
                        print("[LOG] JoinViewController requesting world")
                    }
                    if self.yarnPosition == nil{
                        self.netSession.requestYarnPosition()
                        print("[LOG] JoinViewController requesting yarn position")
                    }
                    if (self.yarn != nil) && (self.world != nil) && (self.yarnPosition != nil) {
                        print("[LOG] JoinViewController segueing")
                        timer.invalidate()
                        DispatchQueue.main.async {
                            self.performSegue(withIdentifier: "ToAR", sender: self)
                        }
                    }
                }
            }
            connection = true
        }
    }
    
    func connectionRequest(from peer: MCPeerID, to session: MCSession, handler: @escaping (Bool, MCSession) -> Void) {
        handler(true, session)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.destination is ARViewController{
            connectingAlert?.dismiss(animated: true, completion: nil)
            let dest = segue.destination as! ARViewController
            dest.yarn = yarn
            dest.world = world
            dest.netSession = self.netSession
            dest.isJoining = true
            dest.__yarnPosition = yarnPosition
        }
    }
    
    func setWorld(map: ARWorldMap) {
        world = map
    }
}

extension JoinViewController: UICollectionViewDataSource{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return peerList.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "YarnCell", for: indexPath) as! YarnCollectionViewCell
        cell.yarnName.text = peerList.map({$0.value})[indexPath.row]
        cell.layer.shadowColor = #colorLiteral(red: 0.7843137255, green: 0.5176470588, blue: 0.0862745098, alpha: 1)
        cell.layer.shadowOffset = CGSize(width: -3.0, height: 4.0)
        cell.layer.shadowOpacity = 1.0
        cell.layer.shadowRadius = 5.0
        return cell
    }
}

extension JoinViewController: UICollectionViewDelegate{
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        netSession.invite(peer: peerList.map({$0.key})[indexPath.row])
        connectedYarnName = peerList.map({$0.value})[indexPath.row]
    }
    
    func collectionView(_ collectionView: UICollectionView, canMoveItemAt indexPath: IndexPath) -> Bool {
        return false
    }
}

extension JoinViewController: YarnDelegate{
    
    func setYarnPosition(position: ARAnchor) {
        yarnPosition = position
    }
    
    func getPosition() -> ARAnchor {
        return yarnPosition ?? ARAnchor(transform: simd_float4x4(0.1))
    }
    
    func set(hunger: Float, player: MCPeerID) {
        //Do nothing
    }
    
    func set(fun: Float, player: MCPeerID) {
        //Do nothing
    }
    
    func setYarn(data: YarnData) {
        yarn = data
        print("[LOG] \(String(describing: yarn?.userNames))")
    }
    
    func getYarn() -> YarnData? {
        return yarn
    }
}
