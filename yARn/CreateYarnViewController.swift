//
//  CreateYarnViewController.swift
//  yARn
//
//  Created by Alessandro Scala on 11/12/2018.
//  Copyright © 2018 TheLeftSide. All rights reserved.
//

import UIKit

class CreateYarnViewController: UIViewController {

    @IBOutlet weak var yarnName: UITextField!
    @IBOutlet weak var stackViewImageText: UIStackView!
    @IBOutlet weak var stackViewNameButton: UIStackView!
    let layer = UIView()
    
    @IBOutlet weak var yarnView: UIImageView!
    @IBOutlet weak var createButton: UIButton!
    @IBAction func cancel(_ sender: UIButton) {
        
        self.dismiss(animated: true, completion: nil)
        
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        createButton.layer.shadowColor = #colorLiteral(red: 0.7843137255, green: 0.5176470588, blue: 0.0862745098, alpha: 1)
        createButton.layer.shadowOffset = CGSize(width: -3.0, height: 4.0)
        createButton.layer.shadowOpacity = 1.0
        createButton.layer.shadowRadius = 5.0
        
        yarnName.attributedPlaceholder = NSAttributedString(string: "Write name here", attributes: [NSAttributedString.Key.foregroundColor: #colorLiteral(red: 0.9254901961, green: 0.6392156863, blue: 0.1568627451, alpha: 1)])
        
        
        //adding the bar under the textview programmatically
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        let x = self.stackViewImageText.frame.origin.x + self.stackViewNameButton.frame.origin.x + self.yarnName.frame.origin.x
        let y = self.stackViewImageText.frame.origin.y + self.stackViewNameButton.frame.origin.y + self.yarnName.frame.origin.y + self.yarnName.frame.height
        
        self.layer.frame = CGRect(x: x, y: y, width: self.yarnName.frame.width, height: 2)
        self.layer.layer.borderWidth = 1
        self.layer.layer.borderColor = UIColor(red: 0.78, green: 0.52, blue: 0.09, alpha: 1).cgColor
        self.view.addSubview(self.layer)
        
        print("\(self.view.subviews)")
        print("\(self.createButton.frame.height)")
    }
    
    override func viewWillTransition(to size: CGSize, with coordinator: UIViewControllerTransitionCoordinator) {
        super.viewWillTransition(to: size, with: coordinator)
    
        //changing the bar under the textview programmatically
        coordinator.animate(alongsideTransition: nil, completion: {
            _ in
            let x = self.stackViewImageText.frame.origin.x + self.stackViewNameButton.frame.origin.x + self.yarnName.frame.origin.x
            let y = self.stackViewImageText.frame.origin.y + self.stackViewNameButton.frame.origin.y + self.yarnName.frame.origin.y + self.yarnName.frame.height
            self.layer.frame = CGRect(x: x, y: y, width: self.yarnName.frame.width, height: 2)
            self.view.addSubview(self.layer)
        })
        
        
        
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.destination is ARViewController{
            let dest = segue.destination as! ARViewController
            if let name = yarnName.text, name != "" {
                dest.netSession = YarnMultipeerSession(with: .firstHost, yarnName: name, hosting: true)
                dest.netSession.stopAdvertising()
                let yarn = YarnData(name: name, users: [YarnUserData(name: dest.netSession.myPeerID.displayName)])
                dest.yarn = yarn
            }
        }
    }
    
    override func shouldPerformSegue(withIdentifier identifier: String, sender: Any?) -> Bool {
        if identifier == "CreateSegue"{
            if yarnName.text == nil || yarnName.text == ""{
                let dialog = UIAlertController(title: "Ooops ☹️", message: "You forgot to give a name to your yarn!", preferredStyle: .alert)
                dialog.addAction(UIAlertAction(title: "Oh, no, I'll give him one right now", style: .default, handler: {_ in self.yarnName.becomeFirstResponder()}))
                present(dialog, animated: true, completion: nil)
                return false
            }
        }
        return true
    }
}
