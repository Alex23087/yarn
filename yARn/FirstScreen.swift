//
//  ViewController.swift
//  yARn
//
//  Created by Alessandro Scala on 05/12/2018.
//  Copyright © 2018 TheLeftSide. All rights reserved.
//

import UIKit

class FirstScreen: UIViewController {

    @IBOutlet weak var createButton: UIButton!
    @IBOutlet weak var joinButton: UIButton!
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        createButton.layer.shadowColor = #colorLiteral(red: 0.7843137255, green: 0.5176470588, blue: 0.0862745098, alpha: 1)
        createButton.layer.shadowOffset = CGSize(width: -3.0, height: 4.0)
        createButton.layer.shadowOpacity = 1.0
        createButton.layer.shadowRadius = 5.0
        joinButton.layer.shadowColor = #colorLiteral(red: 0.7843137255, green: 0.5176470588, blue: 0.0862745098, alpha: 1)
        joinButton.layer.shadowOffset = CGSize(width: -3.0, height: 4.0)
        joinButton.layer.shadowOpacity = 1.0
        joinButton.layer.shadowRadius = 5.0
        
    }


}

