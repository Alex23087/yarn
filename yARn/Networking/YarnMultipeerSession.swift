//
//  MultipeerSession.swift
//  SessionShare Test
//
//  Created by Alessandro Scala on 06/12/2018.
//  Copyright © 2018 Alessandro Scala. All rights reserved.
//

import MultipeerConnectivity
import ARKit

class YarnMultipeerSession: NSObject {
    static let serviceType = "tls-yarn"
    
    let myPeerID = MCPeerID(displayName: UIDevice.current.name)
    private var session: MCSession!
    private var serviceAdvertiser: MCNearbyServiceAdvertiser!
    private var serviceBrowser: MCNearbyServiceBrowser!
    private var dataQueue: [String: NetAction]!
    private var yarnDataResponses: [String: YarnData]!
    private var state: State = .undefined
    public var delegate: NetworkDelegate?
    public var yarnDelegate: YarnDelegate?
    
    var connectedPeers: [MCPeerID] {
        return session.connectedPeers
    }
    
    enum NetAction: Int, Codable{
        case sendWorld
        case throwCookie
        case setYarnPosition
        case worldRequest
        case yarnDataRequest
        case sendHunger
        case sendFun
        case sendYarnData
        case resetAction
        case connectionEstablished
        case yarnPositionRequest
        //TODO: Add user, remove user
    }
    
    enum State{
        case firstJoin
        case join
        case connected
        case firstHost
        case undefined
    }
    
    init(with state: State = .undefined, yarnName: String, hosting: Bool = false) {
        super.init()
        
        self.state = state
        
        session = MCSession(peer: myPeerID, securityIdentity: nil, encryptionPreference: .required)
        session.delegate = self
        
        serviceAdvertiser = MCNearbyServiceAdvertiser(peer: myPeerID, discoveryInfo: ["Hosting": hosting ? "true" : "false", "YarnName": yarnName], serviceType: YarnMultipeerSession.serviceType)
        serviceAdvertiser.delegate = self
        serviceAdvertiser.startAdvertisingPeer()
        
        serviceBrowser = MCNearbyServiceBrowser(peer: myPeerID, serviceType: YarnMultipeerSession.serviceType)
        serviceBrowser.delegate = self
        serviceBrowser.startBrowsingForPeers()
        
        dataQueue = [:]
    }
    
    private func sendToAllPeers(_ data: Data) {
        do {
            try session.send(data, toPeers: connectedPeers, with: .reliable)
        } catch {
            print("[LOG] Error sending data to peers: \(error.localizedDescription)")
        }
    }
    
    public func setFirstJoinState(){
        state = .firstJoin
    }
    
    public func startAdvertising(){
        serviceAdvertiser.startAdvertisingPeer()
    }
    
    public func stopAdvertising(){
        serviceAdvertiser.stopAdvertisingPeer()
    }
}

extension YarnMultipeerSession: MCSessionDelegate {
    
    func session(_ session: MCSession, peer peerID: MCPeerID, didChange state: MCSessionState) {
        delegate?.peerCountChanged(count: self.connectedPeers.count)
    }
    
    func session(_ session: MCSession, didReceive data: Data, fromPeer peerID: MCPeerID) {
        if let action = dataQueue[peerID.displayName]{
            switch action{
            case .sendWorld:
                guard let world = try! NSKeyedUnarchiver.unarchivedObject(ofClass: ARWorldMap.self, from: data) else {fatalError("[LOG] Error unpacking world")}
                print("[LOG] Setting World")
                delegate?.setWorld(map: world)
            case .throwCookie:
                guard let cookie = try? JSONDecoder().decode(CookieThrow.self, from: data) else {
                    print("[LOG] Error unpacking cookie throw data")
                    dataQueue[peerID.displayName] = nil
                    return
                }
                print("[LOG] Throwing cookie")
                delegate?.throwCookie(action: cookie)
            case .setYarnPosition:
                guard let anchor = try! NSKeyedUnarchiver.unarchivedObject(ofClass: ARAnchor.self, from: data) else {fatalError("[LOG] Failed unpacking Yarn anchor data")}
                print("[LOG] Setting Yarn position")
                delegate?.setYarnPosition(position: anchor)
            case .worldRequest:
                print("[LOG] World request received")
                delegate?.getWorld(callback: worldReceived)
            case .sendHunger:
                print("[LOG] Getting hunger level")
                guard let hungerLevel = try? NSKeyedUnarchiver.unarchiveTopLevelObjectWithData(data) as! Float else {print("[LOG] Error setting hunger value"); break}
                yarnDelegate?.set(hunger: hungerLevel, player: peerID)
            case .sendFun:
                print("[LOG] Getting fun level")
                guard let funLevel = try? NSKeyedUnarchiver.unarchiveTopLevelObjectWithData(data) as! Float else {print("[LOG] Error setting hunger value"); break}
                yarnDelegate?.set(fun: funLevel, player: peerID)
            case .sendYarnData:
                print("[LOG] Received yarn data")
                guard let yarn = try? JSONDecoder().decode(YarnData.self, from: data) else {
                    fatalError("[LOG] Failed to decode yarn data")
                }
                getting(yarn: yarn, from: peerID)
            case .resetAction:
                print("[LOG] Resetting action for peer: \(peerID.displayName)")
                dataQueue[peerID.displayName] = nil
            case .yarnDataRequest:
                print("[LOG] Receiving yarn data request")
                guard let yarn = yarnDelegate?.getYarn() else {break}
                send(yarn: yarn)
            case .connectionEstablished:
                print("[LOG] Connection established")
                //if delegate is PeerBrowserDelegate{
                    print("[LOG] connectionEstablished() called")
                    (delegate as? PeerBrowserDelegate)?.connectionEstablished()
                //TODO: Set the previous line as optional
                //}
            case .yarnPositionRequest:
                print("[LOG] yarnpositionrequest")
                send(yarnPosition: (yarnDelegate?.getPosition())!)
            }
            dataQueue[peerID.displayName] = nil
        }else{
            do{
                guard let action = try NSKeyedUnarchiver.unarchiveTopLevelObjectWithData(data) else { print("[LOG] Error getting peer action"); return }
                let act = NetAction(rawValue: action as! Int)
                print("[LOG] action: \(String(describing: act))")
                if act == NetAction.worldRequest{
                    print("[LOG] World request received")
                    delegate?.getWorld(callback: worldReceived)
                }else if act == NetAction.connectionEstablished{
                    print("[LOG] Connection established")
                    //if delegate is PeerBrowserDelegate{
                        print("[LOG] connectionEstablished() called")
                        (delegate as? PeerBrowserDelegate)?.connectionEstablished()
                    //TODO: Set the previous line as optional
                    //}
                }else{
                    dataQueue[peerID.displayName] = act
                    print(dataQueue)
                }
            }catch{
                print(error.localizedDescription)
            }
        }
    }
    
    func session(_ session: MCSession, didReceive stream: InputStream, withName streamName: String, fromPeer peerID: MCPeerID) {
        fatalError("[LOG] This service does not send/receive streams.")
    }
    
    func session(_ session: MCSession, didStartReceivingResourceWithName resourceName: String, fromPeer peerID: MCPeerID, with progress: Progress) {
        fatalError("[LOG] This service does not send/receive resources.")
    }
    
    func session(_ session: MCSession, didFinishReceivingResourceWithName resourceName: String, fromPeer peerID: MCPeerID, at localURL: URL?, withError error: Error?) {
        fatalError("[LOG] This service does not send/receive resources.")
    }
    
}

extension YarnMultipeerSession: MCNearbyServiceBrowserDelegate {
    
    public func browser(_ browser: MCNearbyServiceBrowser, foundPeer peerID: MCPeerID, withDiscoveryInfo info: [String: String]?) {
        let hosting = (info?["Hosting"] ?? "false") == "true" ? true : false
        let yarnName = info?["YarnName"] ?? ""
        if state == State.firstJoin{
            delegate?.foundPeer(peerID: peerID, hosting: hosting, yarnName: yarnName)
        }
        if delegate?.shouldInvite(peer: peerID, hosting: hosting, yarnName: yarnName) ?? false{
            browser.invitePeer(peerID, to: session, withContext: nil, timeout: 10)
            print("[LOG] Invited peer \(peerID.displayName)")
        }
    }
    
    public func browser(_ browser: MCNearbyServiceBrowser, lostPeer peerID: MCPeerID) {
        if state == State.firstJoin{
            delegate?.lostPeer(peerID: peerID)
        }
    }
    
    public func invite(peer: MCPeerID){
        self.serviceBrowser?.invitePeer(peer, to: self.session, withContext: nil, timeout: 10)
        if state == .firstJoin{
            synchronizeYarnData()
        }
    }
    
}

extension YarnMultipeerSession: MCNearbyServiceAdvertiserDelegate {
    
    func advertiser(_ advertiser: MCNearbyServiceAdvertiser, didReceiveInvitationFromPeer peerID: MCPeerID, withContext context: Data?, invitationHandler: @escaping (Bool, MCSession?) -> Void) {
        delegate?.connectionRequest(from: peerID, to: self.session){accept, session in
            if accept{
                print("[LOG] Accepting invitation from \(peerID.displayName)")
                self.delegate?.peerCountChanged(count: self.connectedPeers.count+1)
            }else{
                print("[LOG] Declining invitation from \(peerID.displayName)")
            }
            invitationHandler(accept, session)
        }
    }
    
}

extension YarnMultipeerSession{     //Actions
    func send(worldMap: ARWorldMap){
        do{
            sendToAllPeers(try NSKeyedArchiver.archivedData(withRootObject: NetAction.sendWorld.rawValue, requiringSecureCoding: false))
            sendToAllPeers(try NSKeyedArchiver.archivedData(withRootObject: worldMap, requiringSecureCoding: true))
        }catch{
            print(error.localizedDescription)
            fatalError("[LOG] Fatal error encoding world")
        }
    }
    
    func send(cookieThrow: CookieThrow){
        do{
            sendToAllPeers(try NSKeyedArchiver.archivedData(withRootObject: NetAction.throwCookie.rawValue, requiringSecureCoding: false))
            sendToAllPeers(try JSONEncoder().encode(cookieThrow))
        }catch{
            print(error.localizedDescription)
            fatalError("[LOG] Fatal error encoding cookie throw")
        }
    }
    
    func send(yarnPosition: ARAnchor){
        do{
            sendToAllPeers(try NSKeyedArchiver.archivedData(withRootObject: NetAction.setYarnPosition.rawValue, requiringSecureCoding: false))
            sendToAllPeers(try NSKeyedArchiver.archivedData(withRootObject: yarnPosition, requiringSecureCoding: true))
        }catch{
            print(error.localizedDescription)
            fatalError("[LOG] Error sending Yarn position")
        }
    }
    
    func send(hunger: Float){
        do{
            sendToAllPeers(try NSKeyedArchiver.archivedData(withRootObject: NetAction.sendHunger.rawValue, requiringSecureCoding: false))
            sendToAllPeers(try NSKeyedArchiver.archivedData(withRootObject: hunger, requiringSecureCoding: true))
        }catch{
            print(error.localizedDescription)
            print("[LOG] Error sending hunger level")
        }
    }
    
    func send(fun: Float){
        do{
            sendToAllPeers(try NSKeyedArchiver.archivedData(withRootObject: NetAction.sendFun.rawValue, requiringSecureCoding: false))
            sendToAllPeers(try NSKeyedArchiver.archivedData(withRootObject: fun, requiringSecureCoding: true))
        }catch{
            print(error.localizedDescription)
            print("[LOG] Error sending fun level")
        }
    }
    
    func synchronizeYarnData(){
        do{
            sendToAllPeers(try NSKeyedArchiver.archivedData(withRootObject: NetAction.yarnDataRequest.rawValue, requiringSecureCoding: false))
            yarnDataResponses = [:]
            yarnDataResponses[self.myPeerID.displayName] = yarnDelegate?.getYarn()
        }catch{
            print(error.localizedDescription)
            fatalError("[LOG] Error requesting yarn data")
        }
    }
    
    func requestWorld(){
        do{
            try session.send((try NSKeyedArchiver.archivedData(withRootObject: NetAction.worldRequest.rawValue, requiringSecureCoding: false)), toPeers: [connectedPeers.first!], with: .reliable)
            print("[LOG] requesting world")
        }catch{
            print(error.localizedDescription)
            fatalError("[LOG] Error requesting world data")
        }
    }
    
    func send(yarn: YarnData){
        do{
            sendToAllPeers(try NSKeyedArchiver.archivedData(withRootObject: NetAction.sendYarnData.rawValue, requiringSecureCoding: false))
            sendToAllPeers(try JSONEncoder().encode(yarn))
        }catch{
            print(error.localizedDescription)
            fatalError("[LOG] Error sending yarn data")
        }
    }
    
    private func sendResetAction(){
        do{
            sendToAllPeers(try NSKeyedArchiver.archivedData(withRootObject: NetAction.resetAction.rawValue, requiringSecureCoding: false))
        }catch{
            print(error.localizedDescription)
            fatalError("[LOG] Error sending resetAction signal")
        }
    }
    
    public func sendConnectionEstablished(){
        do{
            sendToAllPeers(try NSKeyedArchiver.archivedData(withRootObject: NetAction.connectionEstablished.rawValue, requiringSecureCoding: false))
        }catch{
            print(error.localizedDescription)
            fatalError("[LOG] Error sending connection established signal")
        }
    }
    
    public func requestYarnPosition(){
        do{
            sendToAllPeers(try NSKeyedArchiver.archivedData(withRootObject: NetAction.yarnPositionRequest.rawValue, requiringSecureCoding: false))
        }catch{
            print(error.localizedDescription)
            fatalError("[LOG] Error sending yarnPositionRequest signal")
        }
    }
    
    private func getting(yarn: YarnData, from user: MCPeerID){
        if yarnDataResponses?[user.displayName] != nil{
            return
        }
        yarnDataResponses[user.displayName] = yarn
        if state == .firstJoin{
            print("[LOG] called getting() with .firstJoin")
            yarnDelegate?.setYarn(data: yarnDataResponses.first!.value)
            serviceBrowser.stopBrowsingForPeers()
            serviceBrowser.startBrowsingForPeers()
            serviceAdvertiser.startAdvertisingPeer()
            state = .connected
            /*if delegate is PeerBrowserDelegate{
                (delegate as! PeerBrowserDelegate).connectionEstablished()
            }*/
        }
        if yarnDataResponses.count == yarnDataResponses?[myPeerID.displayName]?.users.count{
            //yarnDelegate?.setYarn(data: yarnDataResponses.map({$0.value}).getNewest())
        }
    }
    
    private func worldReceived(world: ARWorldMap){
        send(worldMap: world)
        print("[LOG] Sending world")
    }
}


public protocol NetworkDelegate{
    func setWorld(map: ARWorldMap)
    func getWorld(callback: @escaping (ARWorldMap) -> Void)
    func throwCookie(action: CookieThrow)
    func setYarnPosition(position: ARAnchor)
    func connectionRequest(from peer: MCPeerID, to session: MCSession, handler: @escaping (Bool, MCSession) -> Void)
    func shouldInvite(peer: MCPeerID, hosting: Bool, yarnName: String) -> Bool
    func peerCountChanged(count: Int)
    func foundPeer(peerID: MCPeerID, hosting: Bool, yarnName: String)
    func lostPeer(peerID: MCPeerID)
}

public protocol GameNetworkDelegate: NetworkDelegate{}
public protocol PeerBrowserDelegate: NetworkDelegate{
    func connectionEstablished()
}

extension GameNetworkDelegate{  //Extension for the AR scene. These methods are optional for that
    func connectionRequest(from peer: MCPeerID, to session: MCSession, handler: @escaping (Bool, MCSession) -> Void) {handler(true,session)}
    func shouldInvite(peer: MCPeerID, hosting: Bool, yarnName: String) -> Bool {return true}
    func peerCountChanged(count: Int) {}
    func foundPeer(peerID: MCPeerID, hosting: Bool, yarnName: String) {}
    func lostPeer(peerID: MCPeerID) {}
}

extension PeerBrowserDelegate{  //Extension for the first time join screen. These methods are optional for that
    func setWorld(map: ARWorldMap) {}
    func getWorld(callback: @escaping (ARWorldMap) -> Void) {}
    func throwCookie(action: CookieThrow) {}
    func setYarnPosition(position: ARAnchor) {}
    func connectionRequest(from peer: MCPeerID, to session: MCSession, handler: @escaping (Bool, MCSession) -> Void) {
        handler(shouldInvite(peer: peer, hosting: false, yarnName: ""), session)        //TODO: maybe we have to set right parameters here
    }
    func shouldInvite(peer: MCPeerID, hosting: Bool, yarnName: String) -> Bool {return false}
}

protocol YarnDelegate{  //Delegate to set yarn's values
    func set(hunger: Float, player: MCPeerID)
    func set(fun: Float, player: MCPeerID)
    func setYarn(data: YarnData)
    func getYarn() -> YarnData?
    func getPosition() -> ARAnchor
}
